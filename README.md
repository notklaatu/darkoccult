# Dark Cults: oCCult Edition

**Dark Cults** is a game from the early 80s by Kenneth Rahman,
published by **Dark House**. It is out-of-print and mostly
forgotten.

It is a story-telling game; a little like an RPG without the game
master.

In **Dark Cults**, one player takes on the role as DEATH and the other
player takes on the role of LIFE. They compete by spinning a tale, as
dictated by the cards they play, in an attempt to either kill or save
(respectively) the main character of their story.


## So What's the oCCult Edition?

**Dark Cults** is a fun and creative game, but being old and
out-of-print, it is basically impossible to obtain. Sure, some people
have scanned the cards and [posted them
online](http://darkcults.homestead.com/), but they are low-resolution
and pretty heavy on ink. So this is a print-and-play edition of the
game with:

* re-designed cards based on the content of the original 1983
  deck, but with an Edward Gorey vintage feel (using Creative Commons
  vintage artwork)

* a few new cards for variety

* some minor changes to one or two original cards due to artwork
  availability

* the **Object** add-on deck from 1985

* a re-formatted rulebook combining and integrating the original 1983
  rulebook with text from the 1985 add-on rulebook, plus the
  single-player and multi-player modes

* quickstart guide

* a new **oCCult Edition** rule variant and some character build
  sheets from openD6, to incorporates RPG elements into the
  storytelling


## License

The rules and text of the cards are copyrighted by Kenneth Rahman. I
considered making a "clone" originally, but after some attempt at
contacting Kenneth, and some thought about ethics, I decided it would
be *more* respectful to admit that this is **Dark Cults** with mods
rather than claim it's **The Shadow oCCult** (or something like that)
with oddly similar rules.

All of the assets are licensed
[CC-0](http://creativecommons.org/publicdomain/zero/1.0) (the art is
from [openclipart.org](http://openclipart.org), a CC-0 art site),
except the content derived from OpenD6, which is licensed [Open Game
License](https://gitlab.com/everylicense/everylicense/raw/master/ogl_openGameLicense/OGLv1.0a.txt)
(OGL).
