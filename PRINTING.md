# How to Print

There are four ways to print this game: easy, automatic, manual, and
monkey.

## Monkey printing

This is super easy. It's so easy a monkey could do it. It doesn't
exist yet, but I do have plans to make it a reality.


## Easy printing

1. Download the PDF of the game and print it on card stock. Remember to
print both the front and the back.

2. Cut out each cards.

3. That was easy.


## Printing Manually

1. Download and install [Inkscape](http://inkscape.org)

2. Open 9 cards in Inkscape and layout evenly on a page

3. Print

Rinse and repeat...


## Auto Printing

This card deck was made entirely with open source software on an open
source operating system, so the only way I have tested an automated
build is on Linux and BSD.

Linux is free to use, so if you're up for it, just boot Linux, build,
and print. If you don't want to boot into Linux or BSD to build the
deck but are handy with scripting on your platform, feel free to
modify my build script for your own use. And if you do that, please do
send that script to me so that I can add it to this repo for others to
use.

Basically, there are three steps: get the software needed to build,
run the build script, and print.

In that order:

1. Get the Software

   * [Linux](https://linux.com/learn/5-live-linux-desktop-distributions-you-should-know)
   * [bash shell](https://gnu.org/software/bash)
   * [Inkscape](http://inkscape.org)
   * [GNU parallel](https://www.gnu.org/software/parallel/)
   * [pdfjam](http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam)
   * [ImageMagick](http://imagemagick.org/script/index.php)
   * xcftools
   * [pdf-stapler](https://github.com/hellerbarde/stapler)
   * Ubuntu font
   * Orbitron font
   * Essays1743 font

   For docs:

   * docbook
   * fop
   * xsltproc
   * xmlto

2. Run the build script:

       $ sh ./builder.sh build .

3. Print. The printable pages are in a new directory called
   `print`. Remember to print both the front and the back!

