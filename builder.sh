#!/bin/sh
# bash script to build Dark oCCult for printing

# GNU All-Permissive License
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

VILE="tee"
BLD=${BLD:-build}
DEST=${DEST:-print}
RES=${RES:-600}
XARGLIKE=parallel
PAPER=`grep -v '^$\|^\s*\#' /etc/papersize`

help() {
    echo "builder.sh [build|deck|docs|merge|clean] /path/to/dir" | $VILE
    echo "build Dark oCCult for printing" | $VILE
    echo " "
    echo "./builder.sh build . -> build printable deck and docs from current directory."
    echo "Paper size defaults to A4. Change it with:"
    echo "./builder.sh --paper letter build ."
    echo "./builder.sh --paper a4 build ."
    echo " "
    echo "Output dir defaults to build. Set it with:"
    echo "BLD=build-letter ./builder.sh --paper letter build ." 
    echo " "
    echo "Individual stages:"
    echo "builder.sh deck /path/to/dark/oCCult/dir -> build card set for printing" | $VILE
    echo "builder.sh deck merge /path/to/dark/oCCult/dir -> build to one PDF file" | $VILE
    echo "builder.sh docs /path/to/dark/oCCult/dir  -> build docs for printing" | $VILE
    echo "builder.sh clean /path/to/dark/oCCult/dir -> start over" | $VILE
    echo " "
    exit
}

merge() {
    cd "${CWD}"/"${BLD}"/"${DEST}"

    pdf-stapler cat `ls 0*pdf | "${XARGLIKE}" -I% --max-args=1 echo % back.pdf` print-deck.pdf

    echo "Combined into one document for easy printing."
    mv 0??.pdf _trash
    mv back.pdf _trash
}

sane() {

# do we have the fonts?
fc-list | grep -i "essays1743" || { exit "Essays1743 font missing. Install from the fonts directory and try again." ; exit; }
fc-list | grep -i "orbitron" || { echo "Orbitron font missing. Install from the fonts directory and try again."; exit; }
fc-list | grep -i "ubuntu" || { echo "Ubuntu font missing. Install from the fonts directory and try again."; exit; }
echo "all fonts accounted for"

#sanity checks
PDFNUP=${PDFNUP:-`which pdfnup`} || PDFNUP=""
CONVERT=${CONVERT:-`which convert`} || CONVERT=${CONVERT:-`which gm` convert}
MONTAGE=${MONTAGE:-`which montage`} || MONTAGE=${MONTAGE:-`which gm` montage}
MOGRIFY=${MOGRIFY:-`which mogrify`} || MOGRIFY=${MOGRIFY:-`which gm` mogrify}
if [ X"$CONVERT" = "X" -o X"$MONTAGE" = "X" ]; then
    echo "ImageMagick or GraphicsMagick is required, but cannot be found."
    exit 1
fi
}

deck() {
echo "Working in $CWD" 

# convert svg to png
mkdir -p "$CWD"/"${BLD}"/"${DEST}"

echo "Exporting with Inkscape..." #debug
find "${CWD}"/deck -type f -iname "*.svg" | "${XARGLIKE}" -I% inkscape -z -e %.png -w 1464 -h 2078 --export-dpi $RES %

#convert xcf player cards
for i in "${CWD}"/deck/player/*xcf; do
    xcf2png $i > "${CWD}"/"${BLD}"/"${DEST}"/`basename $i .xcf`.png
    #"${MOGRIFY}" -rotate 90 -density $RES -geometry 2078x1464 "${CWD}"/"${BLD}"/"${DEST}"/`basename $i .xcf`.png
done

#trim excess cards to maximize printables
mv "${CWD}"/"${BLD}"/"${DEST}"/opend6*paranormal*g \
   "${CWD}"/"${BLD}"/"${DEST}"/player_paranormal.svg.png

#gather all pngs
find "${CWD}"/deck -type f -name "*_*.png" | "${XARGLIKE}" -I% mv % "${CWD}"/"${BLD}"/"${DEST}"
find "${CWD}"/deck/system -type f -name "title*svg.png" | "${XARGLIKE}" -I% mv % "${CWD}"/"${BLD}"/"${DEST}"
find "${CWD}"/"${BLD}"/"${DEST}" -type f -name "*png" | "${XARGLIKE}" "${MOGRIFY}" -rotate 90

# this is where the pngs live
mkdir -p "${CWD}"/"${BLD}"/"${DEST}"/_trash 2> /dev/null

cd "${CWD}"/"${BLD}"/"${DEST}"

#layout
echo "Using $PDFNUP to build card layout."
ls *svg.png | "${XARGLIKE}" --max-args=9 "${PDFNUP}" --papersize \'\{$W,$H\}\' --no-tidy --nup 3x3 --suffix '3x3'

echo "Creating optional player cards."
ls opend6*ng | "${XARGLIKE}" --max-args=9 "${PDFNUP}" --papersize \'\{$W,$H\}\' --nup 3x3 --suffix '3x3'

#rename
n=1
for i in *pdf; do
    echo $i `printf "%03d" $n`.pdf
    mv $i `printf "%03d" $n`.pdf
    let n++
done

#back sheet
cat "${CWD}"/deck/system/back.png > "${CWD}"/"${BLD}"/"${DEST}"/back.png
"${MOGRIFY}" -verbose -rotate 90 "${CWD}"/"${BLD}"/"${DEST}"/back.png

# gen back
"${PDFNUP}" --nup 3x3 --papersize \{$W,$H\} --suffix '3x3' \
	    back.png back.png back.png \
	    back.png back.png back.png \
	    back.png back.png back.png

mv back*pdf back.pdf

echo "Print-spreads generated as single signature documents in the print folder." | $VILE
}

tidy() {
#clean up
mv "${CWD}"/"${BLD}"/"${DEST}"/back.png "${CWD}"/"${BLD}"/"${DEST}"/_trash/ 2>/dev/null
mv "${CWD}"/"${BLD}"/"${DEST}"/*png "${CWD}"/"${BLD}"/"${DEST}"/_trash/ 2>/dev/null
mv *_*3x3*pdf "${CWD}"/"${BLD}"/"${DEST}"/_trash/ 2>/dev/null
echo "${CWD}"
/bin/rm "${CWD}"/doc/source/images/*.jpeg 2>/dev/null
/bin/rm -rf "${CWD}"/"${BLD}"/"${DEST}"/_trash/
}

doc() {
    pushd "${CWD}"/doc
    echo "Using doc dir" `pwd`
    mkdir -p "$CWD"/"${BLD}"/"${DEST}" 2>/dev/null || true
    mkdir build 2>/dev/null || true
    for i in source/images/*svg ; do
	convert $i -density 300 source/images/`basename $i .svg`.jpeg
    done

    # pdf
    xsltproc --output source/tmp.fo \
	     --stringparam paper.type "${PAPER}" \
	     --stringparam page.width $W \
	     --stringparam page.height $H \
	     --stringparam redist.text "no" \
	     --stringparam column.count.titlepage 1 \
	     --stringparam column.count.lot 1 \
	     --stringparam column.count.front 1 \
	     --stringparam column.count.body 2 \
	     --stringparam column.count.back 1 \
	     --stringparam column.count.index 2 \
	     --stringparam body.font.family "TeX Gyre Bonum" \
	     --stringparam title.font.family "Essays1743" \
	     --stringparam bridgehead.font.family "TeX Gyre Bonum" \
	     --stringparam symbol.font.family "UniCons" \
	     --stringparam footer.column.widths "1 0 0" \
	     --stringparam body.font.master 10 \
	     --stringparam body.font.size 10 \
	     --stringparam page.margin.inner .5in \
	     --stringparam page.margin.outer .5in \
	     --stringparam page.margin.top .45in \
	     --stringparam page.margin.bottom .45in \
	     --stringparam title.margin.left 0 \
	     --stringparam title.start.indent 0 \
	     --stringparam body.start.indent 0 \
	     --stringparam chapter.autolabel 0 \
	     style/mystyle.xsl source/docbook.xml
    fop -c style/rego.xml source/tmp.fo build/dark-oCCult.pdf
    /bin/rm source/tmp.fo

    mkdir build/html
    xmlto --skip-validation -o build/html html-nochunks source/docbook.xml
    mv build/html ../"${BLD}"/html-guide
    mv build/ ../"${BLD}"/print/docs

    mkdir ../"${BLD}"/html-guide/images
    cp source/images/*svg ../"${BLD}"/html-guide/images
    sed -i 's_../images_./images_g' ../"${BLD}"/html-guide/docbook.html
    sed -i 's_jpeg_svg_g' ../"${BLD}"/html-guide/docbook.html
    mv ../"${BLD}"/html-guide/docbook.html ../"${BLD}"/html-guide/index.html
}

clean() {
    /bin/rm -r "${CWD}"/"${BLD}"
    /bin/rm -f "${CWD}/doc/build"
    /bin/rm "${CWD}"/doc/source/images/*.jpeg 2>/dev/null
    echo "Everything is clean, and tidy, and back to normal." | $VILE
}

## parse
while [ True ]; do
if [ "$1" = "clean" -o "$1" = "--clean" ]; then
    CLEAN=1
    shift 1
elif [ "$1" = "tidy" -o "$1" = "--tidy" ]; then
    TIDY=1
    shift 1
elif  [ "$1" = "doc" -o "$1" = "--doc" -o "$1" = "docs" -o "$1" = "--docs" ]; then 
    DOC=1
    shift 1
elif  [ "$1" = "deck" -o "$1" = "--deck" -o "$1" = "d" -o "$1" = "-d" ]; then 
    BUILD=1
    shift 1
elif  [ "$1" = "merge" -o "$1" = "--merge" ]; then 
    MERGE=1
    shift 1
elif  [ "$1" = "--no-tidy" ]; then 
    TIDY=0
    shift 1
elif  [ "$1" = "--paper" ]; then 
    PAPER=`echo $2 | tr [:upper:] [:lower:]`
    echo $PAPER
    shift 2
elif  [ "$1" = "evil" -o "$1" = "--vile" -o "$1" = "666" -o "$1" = "redrum" ]; then 
    EVIL=1
    shift 1
elif  [ "$1" = "build" -o "$1" = "--build" -o "$1" = "b" -o "$1" = "-b" ]; then 
    TIDY=1
    CLEAN=1
    BUILD=1
    MERGE=1
    DOC=1
    shift 1
elif  [ "$1" = "help" -o "$1" = "--help" -o "$1" = "-h" -o "$1" = "--options" -o "$1" = "options" -o "$1" = "" ]; then 
    help
    shift 1
else
    break
fi
done

if [ X"${1}" = "X" ]; then
    CWD=`pwd`
    echo "CWD is $CWD"
else
    CWD=`readlink -f "${1}"`
    echo "CWD is $CWD"
fi

## main
if [ X"$CLEAN" = X"1" ]; then
    # clean first
    clean "${CWD}"
fi

if [ X"$PAPER" = "X" ]; then
    PAPER=${PAPER:-a4}
fi

if [ X"$PAPER" = X"letter" ]; then
    W="8.5in"
    H="11in"
elif [ X"$PAPER" = X"a4" ]; then
    W="210mm"
    H="297mm"
fi

if [ X"$EVIL" = X"1" ]; then
    # find out if we're evil
    echo "Something beckons you from the comfort of your home, out into the night..."
    VILE='tr "[A-Za-z]" "[N-ZA-Mn-za-m]"'
fi 

if [ X"$BUILD" = X"1" ]; then
    #then build the cards
    sane
    deck "${CWD}"
fi

if [ X"$MERGE" = X"1" ]; then
    echo "------------------MERGING ---------------------"
    merge
fi

if [ X"$TIDY" = X"1" ]; then
    tidy
fi
    
if [ X"$DOC" = X"1" ]; then
    #and the docs
    cat > "${CWD}"/doc/style/rego.xml << EOF

    <fop version="1.0">
<renderers>
  <renderer mime="application/pdf">
     <fonts>
        <directory recursive="true">$HOME/.local/share/fonts</directory>
        <auto-detect/>
     </fonts>
  </renderer>
</renderers>
</fop>
EOF
    doc "${CWD}"
    #/bin/rm "${CWD}"/doc/style/rego.xml
fi

