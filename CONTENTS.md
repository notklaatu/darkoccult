# Contents

* deck: More than 116 cards, in SVG format. SVG is an open source file
  format for vector drawings.

* fonts: The fonts required to render the card deck.

* doc: Game rules taken from the 1983 rulebook and the 1985 expansion
  set, with some additional notes relating to this edition. Also
  included is a modern quickstart instruction sheet.

* character sheet: taken from
  [OpenD6](https://sites.google.com/site/opend6system/), in case you
  want to add RPG elements to **Dark oCCult**

* printing instructions

* a build script written in BASH, for automated print layout. If you
  are not running Linux or BSD, you can perform the layout manually
  with [Inkscape](http://inkscape.org)
